import java.util.Scanner;

class Fahrkartenautomat
{
	
	
    public static void main(String[] args)
    { 
      while (true) 
      { 
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenEinzeln = 0; 
       double eingezahlterGesamtbetrag = 0;
       double eingeworfeneM�nze = 0;
       double r�ckgabebetrag = 0;
       byte anzahltickets = 0 ;
       double zuZahlenGesamt;
       String fahrkartenAusgeben = null;
       byte ticketAuswahl = 0;
       byte gruppenGr��e = 0;
       
       
       
       //Fahrkartenbestellung
       zuZahlenGesamt = bestellungErfassen(zuZahlenEinzeln, anzahltickets, ticketAuswahl, gruppenGr��e);
       
       //Bezahlung
       eingezahlterGesamtbetrag = fahrkartenBezahlen(eingezahlterGesamtbetrag, eingeworfeneM�nze, zuZahlenGesamt);
       
       // Fahrkartenausgabe
       fahrkartenAusgeben(fahrkartenAusgeben);
       
       System.out.println("\n\n");
      
       //R�ckgeldausgabe
       r�ckgabebetrag = r�ckgeldAusgeben(r�ckgabebetrag, eingezahlterGesamtbetrag, zuZahlenGesamt);

       System.out.println("\nVergessen Sie nicht, die Fahrscheine\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.\n");
       warte(0);
       System.out.println("\n\n");
      }
    
    }
    
    public static double bestellungErfassen(double zuZahlenEinzeln, byte anzahltickets, double ticketAuswahl, byte gruppenGr��e)
    { 
       
    	Scanner tastatur = new Scanner(System.in);
        
    	byte zusatz = 0;
    	double zuZahlenGesamt = 0;
    	int gesamtAnzahlTickets = 0;
    	
    	
    	
    	String[] fahrkartenTyp = new String[10];
    	fahrkartenTyp[0] = "Einzelfahrschein Berlin AB";
    	fahrkartenTyp[1] = "Einzelfahrschein Berlin BC";
    	fahrkartenTyp[2] = "Einzelfahrschein Berlin AB";
    	fahrkartenTyp[3] = "Kurzstrecke";
    	fahrkartenTyp[4] = "Tageskarte Berlin AB";
    	fahrkartenTyp[5] = "Tageskarte Berlin BC";
    	fahrkartenTyp[6] = "Tageskarte Berlin AB";
    	fahrkartenTyp[7] = "Kleingruppen-Tageskarte Berlin AB";
    	fahrkartenTyp[8] = "Kleingruppen-Tageskarte Berlin BC";
    	fahrkartenTyp[9] = "Kleingruppen-Tageskarte Berlin AB";
    	
    	double[] fahrkartenPreise = new double[fahrkartenTyp.length];
    	fahrkartenPreise[0] = 2.90;
    	fahrkartenPreise[1] = 3.30;
    	fahrkartenPreise[2] = 3.60;
    	fahrkartenPreise[3] = 1.90;
    	fahrkartenPreise[4] = 8.60;
    	fahrkartenPreise[5] = 9.00;
    	fahrkartenPreise[6] = 9.60;
    	fahrkartenPreise[7] = 23.50;
    	fahrkartenPreise[8] = 24.30;
    	fahrkartenPreise[9] = 24.90;
    	
    	byte[] fahrkartenAnzahl = new byte [fahrkartenTyp.length];
    	fahrkartenAnzahl[0] = 0;
    	fahrkartenAnzahl[1] = 0;
    	fahrkartenAnzahl[2] = 0;
    	fahrkartenAnzahl[3] = 0;
    	fahrkartenAnzahl[4] = 0;
    	fahrkartenAnzahl[5] = 0;
    	fahrkartenAnzahl[6] = 0;
    	fahrkartenAnzahl[7] = 0;
    	fahrkartenAnzahl[8] = 0;
    	fahrkartenAnzahl[9] = 0;
    	
    	
    	
    	do
    	{
        zuZahlenGesamt  = +(zuZahlenEinzeln * anzahltickets);
    	System.out.println("W�hlen Sie Ihren gew�nschten Fahrschein:");
    	for (int i= 0; i < fahrkartenTyp.length; i++ )
    	{
         System.out.println("  " + fahrkartenTyp[i] + " [" +(i+1) + "]");
    	}
    	System.out.print("  Auswahl verlassen"); // + "[" + (fahrkartenTyp.length +1) + "]");
    	System.out.printf("%21s %n", "[" + (fahrkartenTyp.length + 1) + "]");
    	System.out.print  ("Ihre Wahl: ");
    	
    	
    	
    	do {
    	ticketAuswahl = tastatur.nextByte();
    	if(ticketAuswahl <= 0 || ticketAuswahl >= fahrkartenTyp.length+2)
        {
         System.out.println("Bitte w�hlen Sie eine g�ltige Option aus.");
        }
    	else if (ticketAuswahl >= (fahrkartenTyp.length+1))
        {
         warte(0);
         System.out.print("\n");
     	 System.out.print("Auf Wiedersehen!");
     	 System.exit(0);
        }
    	else if(ticketAuswahl >= 1)
    	{
    	zuZahlenEinzeln = fahrkartenPreise[(int) ticketAuswahl - 1];
    	fahrkartenAnzahl[(int) ticketAuswahl -1] = 1;
    	
    	}
    	}while(ticketAuswahl >= 12 || ticketAuswahl <= 0);
    	
    	
       
        System.out.print("Anzahl der Tickets: ");
        anzahltickets = tastatur.nextByte();
        gesamtAnzahlTickets = gesamtAnzahlTickets +anzahltickets;
        
        
        
         /**if (ticketAuswahl <= 1)
         {
          gesamtEinzeltickets = gesamtEinzeltickets +(einzelTickets * anzahltickets);
         }
         if (ticketAuswahl <= 2)
         {
          gesamtTagestickets = gesamtTagestickets +(tagesTickets * anzahltickets); 
         }
         if (ticketAuswahl <= 3)
         {
          gesamtGruppentickets = gesamtGruppentickets +(gruppenTickets * anzahltickets);
         }**/
         
        System.out.println("Aktuelle Gesamtanzahl an Tickets: " + gesamtAnzahlTickets + " (" +(fahrkartenAnzahl[(int) ticketAuswahl -1] * anzahltickets) + " " + fahrkartenTyp[(int) ticketAuswahl -1]);
        
        if (gesamtAnzahlTickets >= 11)
        {
        
        }
        
        
        while (anzahltickets > 10 || 1 > anzahltickets)
        {
        	System.out.println("Bitte geben Sie eine g�ltige Anzahl ein und beachten Sie, dass h�chstens 10 Tickets gleichzeitig bestellt werden k�nnen.");
        	System.out.print("Anzahl der Tickets: ");
        	anzahltickets = tastatur.nextByte();
        }
        
        System.out.println("M�chten Sie zus�tzliche Fahrscheine zur Bestellung hinzuf�gen?");
        System.out.println("  [1]: Weitere Tickets bestellen");
        System.out.println("  [2]: Mit der Bezahlung fortfahren");
        System.out.println("  [3]: Aktuelle Bestell�bersicht");
        zusatz = tastatur.nextByte();
        
        if(zusatz <= 3)
        {
         System.out.println(anzahltickets + " " + fahrkartenTyp[(int) ticketAuswahl -1]);
        }

        while (zusatz >= 4 || zusatz <= 0)
        {
        	System.out.println("Bitte w�hlen Sie eine g�ltige Option aus.");
        	zusatz = tastatur.nextByte();
        }
        
        zuZahlenGesamt = zuZahlenGesamt + (zuZahlenEinzeln * anzahltickets);
        
    	}
    	while(zusatz <= 1);
    	
       
        
        
        return zuZahlenGesamt;
       
    }
    
    public static double fahrkartenBezahlen(double eingezahlterGesamtbetrag, double eingeworfeneM�nze, double zuZahlenGesamt)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenGesamt)
        {
     	   System.out.printf("Noch zu zahlen: "); System.out.printf("%.2f", zuZahlenGesamt - eingezahlterGesamtbetrag);
     	   System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
             
        }
        return eingezahlterGesamtbetrag;
    }
    
    public static String fahrkartenAusgeben(String fahrkartenAusgeben)
    {
     System.out.println("\nFahrscheine werden ausgegeben");
       warte(0);
       return fahrkartenAusgeben;
    }
    
    public static double r�ckgeldAusgeben(double r�ckgabebetrag, double eingezahlterGesamtbetrag, double zuZahlenGesamt)
    {
    	r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenGesamt;
        if(r�ckgabebetrag > 0.0)
        { 
     	   
     	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.printf("Der R�ckgabebetrag in H�he von "); System.out.printf("%.2f", r�ckgabebetrag); System.out.printf(" EURO\n");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 1.99) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.50;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.20;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.10;
            }
            while(r�ckgabebetrag >= 0.01)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
        return r�ckgabebetrag;
    }
    
    public static void warte(int millisekunde)
    {
     for (millisekunde = 0; millisekunde < 10; millisekunde++)
     {
      System.out.print("=");
      try 
      {
       Thread.sleep(250);
      }
      catch (InterruptedException e)
      {
       e.printStackTrace();
      }
     }
    }
    
   
}
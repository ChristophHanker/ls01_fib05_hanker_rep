import java.util.Scanner;

public class Mittelwert {

			   public static void main(String[] args) {

				  Scanner tastatur = new Scanner(System.in); 
			      double x;
			      double y;
			      double m;
			      
			      System.out.print("Geben Sie eine Zahl ein: ");
			      x = tastatur.nextDouble();
			      
			      System.out.print("Geben Sie eine zweite Zahl ein: ");
			      y = tastatur.nextDouble();
			  
			      m = berechneMittelwert(x,y);
			     
			      
			      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f.\n", x, y, m);
			   }
			   
			   public static double berechneMittelwert(double x, double y) {
				   
				  double mittelwert = (x + y) /2.0;
				  
				  return mittelwert;
			   }
			


	}




public class Aufgabe3 {

	public static void main(String[] args) {
		
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;
		System.out.printf("%-12s", "Fahrenheit"); System.out.printf("|"); System.out.printf("%10s\n", "Celsius");
		System.out.printf("-----------------------\n"); 
		System.out.printf("%-12s", "-20"); System.out.printf("|"); System.out.printf("%10.2f\n", a);
		System.out.printf("%-12s", "-10"); System.out.printf("|"); System.out.printf("%10.2f\n", b);
		System.out.printf("%-12s", "+0"); System.out.printf("|"); System.out.printf("%10.2f\n", c);
		System.out.printf("%-12s", "+20"); System.out.printf("|"); System.out.printf("%10.2f\n", d);
		System.out.printf("%-12s", "+30"); System.out.printf("|"); System.out.printf("%10.2f\n", e);

	}

}

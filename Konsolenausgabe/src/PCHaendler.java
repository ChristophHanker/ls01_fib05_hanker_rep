import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel = null;
		int anzahl = 0;
		double preis = 0;
		double mwst = 0;
		double nettogesamtpreis = 0;

		// Benutzereingaben lesen
		System.out.println("Was m�chten Sie bestellen?");
		artikel = liesString(artikel);

		System.out.println("Geben Sie die Anzahl ein:");
		anzahl = liesInt(anzahl);

		System.out.println("Geben Sie den Nettopreis ein:");
		preis = liesDouble(preis);

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		mwst = liesDouble(mwst);

		// Verarbeiten
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettogesamtpreis);
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
//
		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}
	
	public static String liesString(String text)
	{
		Scanner myScanner = new Scanner(System.in);
		text = myScanner.next();
		return text;
	}
	
	public static int liesInt(int text)
	{
		Scanner myScanner = new Scanner(System.in);
		text = myScanner.nextInt();
		return text;
	}
	
	public static double liesDouble(double text)
	{
		Scanner myScanner = new Scanner(System.in);
		text = myScanner.nextDouble();
		return text;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis)
	{
	    
		double gesamtpreis = anzahl * preis;
	    return gesamtpreis;
	}
}

